package com.bjss.basketcase.dto;

import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.common.ProductType;
import org.junit.jupiter.api.Test;

import static com.bjss.basketcase.dto.ProductTest.PRICE_LIST;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BasketTest {

    @Test
    void should_add_only_one() {
        final BasketItem orange = BasketItem.builder().product(Product.builder().sku(1).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build()).build();
        orange.add().add();
        final Basket basket = Basket.builder().build().addToBasket(orange).addToBasket(orange).addToBasket(orange);
        assertEquals(1, basket.getBasketItems().size());

    }

    @Test
    void should_remove_and_be_empty() {
        final BasketItem orange = BasketItem.builder().product(Product.builder().sku(1).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build()).build();
        orange.add().add().remove();
        final Basket basket = Basket.builder().build().addToBasket(orange).addToBasket(orange).removeFromBasket(orange);
        assertEquals(0, basket.getBasketItems().size());
    }

    @Test
    void should_not_ThrowException() {
        final BasketItem orange = BasketItem.builder().product(Product.builder().sku(1).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build()).build();
        final Basket basket = Basket.builder().build().removeFromBasket(orange);
        assertEquals(0, basket.getBasketItems().size());
    }
}