package com.bjss.basketcase.dto;

import com.bjss.basketcase.dto.common.Price;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.common.ProductType;
import com.bjss.basketcase.dto.common.Range;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductTest {

    public static final List<Price> PRICE_LIST = List.of(
            Price.builder().id(1).dates(new Range(LocalDate.MIN,LocalDate.from(LocalDate.of(2021, Month.JUNE, 15)))).build(),
            Price.builder().id(2).dates(new Range(LocalDate.of(2021, Month.JUNE, 16), LocalDate.of(2021, Month.SEPTEMBER, 24))).build(),
            Price.builder().id(3).dates(new Range(LocalDate.of(2021, Month.SEPTEMBER, 5), LocalDate.MAX)).build()
    );

    @Test
    void should_get_All_Actives() {
        final Optional<Price> active = Product.builder().sku(1).name("apples").productType(ProductType.WEIGHT).prices(PRICE_LIST).build().getActive(LocalDate.now());
        assertEquals(3, active.get().getId());
        final Optional<Price> active2 = Product.builder().sku(2).name("pineapple").productType(ProductType.WEIGHT).prices(PRICE_LIST).build().getActive(LocalDate.from(LocalDate.of(2021, Month.JUNE, 17)));
        assertEquals(2, active2.get().getId());
        final Optional<Price> active3 = Product.builder().sku(3).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build().getActive(LocalDate.from(LocalDate.of(2020, Month.OCTOBER, 17)));
        assertEquals(1, active3.get().getId());
    }
}