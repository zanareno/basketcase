package com.bjss.basketcase.dto;

import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.common.ProductType;
import org.junit.jupiter.api.Test;

import static com.bjss.basketcase.dto.ProductTest.PRICE_LIST;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BasketItemTest {

    @Test
    void should_add() {
        final BasketItem orange = BasketItem.builder().product(Product.builder().sku(1).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build()).build();
        orange.add().add();
        assertEquals(3, orange.getQuantity());

    }

    @Test
    void should_remove() {
        final BasketItem orange = BasketItem.builder().product(Product.builder().sku(1).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build()).build();
        orange.add().add().remove();
        assertEquals(2, orange.getQuantity());
    }

    @Test
    void should_not_remove() {
        final BasketItem orange = BasketItem.builder().product(Product.builder().sku(1).name("orange").productType(ProductType.WEIGHT).prices(PRICE_LIST).build()).build();
        orange.remove();
        assertEquals(1, orange.getQuantity());
    }
}