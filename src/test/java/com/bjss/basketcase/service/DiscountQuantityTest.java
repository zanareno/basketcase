package com.bjss.basketcase.service;

import com.bjss.basketcase.config.SetupStore;
import com.bjss.basketcase.dto.Basket;
import com.bjss.basketcase.dto.BasketItem;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.offers.Offer;
import com.bjss.basketcase.dto.offers.OfferDiscountByProductQuantity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class DiscountQuantityTest {

    static final Map<String, Product> products = SetupStore.readProducts();
    static final Collection<OfferDiscountByProductQuantity> discounts = new ArrayList<>();
    static final Basket basket = Basket.builder().build();

    @BeforeAll
    static void setupAll(){
        final Product soup = products.get("Soup");
        final Product bread = products.get("Bread");
        final OfferDiscountByProductQuantity productBreadQuantity = OfferDiscountByProductQuantity.builder().product(bread).quantity(2).build();
        productBreadQuantity.setOffer(Offer.builder().discount(50).product(soup).name("2 Breads, 50% off in soup").build());

        final OfferDiscountByProductQuantity productSoupQuantity = OfferDiscountByProductQuantity.builder().product(soup).quantity(2).build();
        productSoupQuantity.setOffer(Offer.builder().discount(50).product(bread).name("2 Soups, 50% off in bread").build());

        discounts.addAll( List.of(productBreadQuantity, productSoupQuantity));
        basket.addToBasket(BasketItem.builder().product(products.get("Bread")).quantity(2).build());
        basket.addToBasket(BasketItem.builder().product(products.get("Soup")).quantity(2).build());
    }

    DiscountQuantity discountQuantity;


    @BeforeEach
    public void setup(){
        discountQuantity = new DiscountQuantity();
        discountQuantity.read(discounts);
    }

    @Test
    void should_read() {
        assertEquals(2, discountQuantity.getOffersMap().size());
    }

    @Test
    void should_not_selectOffer() {
        final BasketItem soup = BasketItem.builder().product(products.get("Soup")).build();
        Basket basket = Basket.builder().build().addToBasket(soup);
        final Optional<OfferDiscountByProductQuantity> breadOffer = discountQuantity.selectOffer(basket);
        assertEquals(Optional.empty(), breadOffer);
    }

    @Test
    void should_selectOffer() {
        final Optional<OfferDiscountByProductQuantity> soupOffer = discountQuantity.selectOffer(basket);
        assertEquals(50, soupOffer.get().getOffer().getDiscount());
        assertEquals("Soup", soupOffer.get().getOffer().getProduct().getName());
        assertEquals("Bread", soupOffer.get().getProduct().getName());
        assertEquals(2, soupOffer.get().getQuantity());
    }


    @Test
    void should_apply_Offer() {
        final Optional<OfferDiscountByProductQuantity> soupOffer = discountQuantity.selectOffer(basket);
        discountQuantity.apply(basket, soupOffer);
        assertEquals(BigDecimal.valueOf(32.5), soupOffer.get().getOffer().getValueWithDiscount());
    }
}