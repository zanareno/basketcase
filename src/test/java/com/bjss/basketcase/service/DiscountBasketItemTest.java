package com.bjss.basketcase.service;

import com.bjss.basketcase.config.SetupStore;
import com.bjss.basketcase.dto.BasketItem;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.common.ProductType;
import com.bjss.basketcase.dto.common.Range;
import com.bjss.basketcase.dto.offers.Offer;
import com.bjss.basketcase.dto.offers.OfferDateDiscount;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiscountBasketItemTest {

    static final Map<String, Product> products = SetupStore.readProducts();
    static final Collection<OfferDateDiscount> discounts = new ArrayList<>();

    @BeforeAll
    static void setupAll(){
        final OfferDateDiscount applesDiscount = OfferDateDiscount.builder()
                .timeRange(new Range<>(
                        LocalDateTime.now().minusDays(1),
                        LocalDateTime.now().plusWeeks(1)))
                .build();
        final Product apples = products.get("Apples");
        applesDiscount.setOffer(Offer.builder().discount(10).product(apples).name("Apples 10% off").build());

        final OfferDateDiscount orangeDiscount = OfferDateDiscount.builder()
                .timeRange(new Range<>(
                        LocalDateTime.now().minusDays(1),
                        LocalDateTime.now().plusWeeks(1)))
                .build();
        final Product oranges = products.get("Oranges");
        orangeDiscount.setOffer(Offer.builder().discount(10).product(oranges).name("Oranges 10% off").build());
        discounts.addAll( List.of(applesDiscount, orangeDiscount));
    }

    DiscountBasketItem discountBasketItem;

    @BeforeEach
    public void setup(){
        discountBasketItem = new DiscountBasketItem();
        discountBasketItem.read(discounts);
    }

    @Test
    void should_read() {
        assertEquals(2, discountBasketItem.getOffersMap().size());
    }

    @Test
    void should_selectOffer() {
        final BasketItem oranges = BasketItem.builder().product(products.get("Oranges")).build();
        final Optional<OfferDateDiscount> orangesOffer = discountBasketItem.selectOffer(oranges);
        assertEquals(10, orangesOffer.get().getOffer().getDiscount());
        assertEquals(ProductType.WEIGHT, orangesOffer.get().getOffer().getProduct().getProductType());
    }

    @Test
    void should_apply_Offer() {
        final BasketItem oranges = BasketItem.builder().product(products.get("Oranges")).quantity(3).build();
        final Optional<OfferDateDiscount> orangesOffer = discountBasketItem.selectOffer(oranges);
        discountBasketItem.apply(oranges, orangesOffer);
        assertEquals(10, orangesOffer.get().getOffer().getDiscount());
        assertEquals(BigDecimal.TEN, orangesOffer.get().getOffer().getValueWithDiscount());
    }
}