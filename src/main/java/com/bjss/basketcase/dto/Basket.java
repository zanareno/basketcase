package com.bjss.basketcase.dto;

import com.bjss.basketcase.dto.common.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Basket {

    private Map<Product, BasketItem> basketItems;

    public Basket addToBasket(final BasketItem item){
        if(basketItems == null){
            basketItems = new HashMap();
        }
        basketItems.putIfAbsent(item.getProduct(), item);
        return this;
    }
    public Basket removeFromBasket(final BasketItem item){
        if(basketItems == null){
            basketItems = emptyMap();
            return this;
        }
        basketItems.remove(item.getProduct(), item);
        return this;
    }

}