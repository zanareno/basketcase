package com.bjss.basketcase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
public class NullReceipt extends AbstractReceipt{
    @Override
    public String toString() {
        return "No items in the basket. Nothing to process!";
    }
}