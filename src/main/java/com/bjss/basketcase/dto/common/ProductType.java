package com.bjss.basketcase.dto.common;

public enum ProductType {

    WEIGHT, UNIT
}