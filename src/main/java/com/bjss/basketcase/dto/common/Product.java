package com.bjss.basketcase.dto.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "name")
public class Product {

    private long sku;
    @NonNull
    private String name;
    @NonNull
    private ProductType productType;
    @NonNull
    private List<Price> prices;

    public Optional<Price> getActive(final LocalDate reference){
        return prices.stream().filter(price -> reference.isAfter(price.getDates().getStart()) && reference.isBefore(price.getDates().getEnd())).findFirst();
    }
}