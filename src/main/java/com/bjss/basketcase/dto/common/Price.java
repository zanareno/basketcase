package com.bjss.basketcase.dto.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Price {

    private int id;
    private String description;
    @NonNull
    private Range<LocalDate> dates;
    private BigDecimal value;
}