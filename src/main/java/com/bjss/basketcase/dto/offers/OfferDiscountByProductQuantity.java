package com.bjss.basketcase.dto.offers;

import com.bjss.basketcase.dto.common.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OfferDiscountByProductQuantity extends BaseOffer {
    private Product product;
    private int quantity;
}