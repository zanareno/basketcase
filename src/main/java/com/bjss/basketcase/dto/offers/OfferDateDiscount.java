package com.bjss.basketcase.dto.offers;

import com.bjss.basketcase.dto.common.Range;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OfferDateDiscount extends BaseOffer{

    private Range<LocalDateTime> timeRange;
}