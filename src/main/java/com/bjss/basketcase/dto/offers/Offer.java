package com.bjss.basketcase.dto.offers;

import com.bjss.basketcase.dto.common.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Offer {
    private String name;
    private Product product;
    private int discount;
    private transient BigDecimal valueWithDiscount;
}