package com.bjss.basketcase.dto;

import com.bjss.basketcase.dto.common.Product;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(of = "product")
public class BasketItem {

    private Product product;
    private int quantity;

    public BasketItem(final Product product){
        this.product = product;
        this.quantity = 1;
    }

    public BasketItem(final Product product, final int quantity) {
        this.product = product;
        this.quantity = Math.max(quantity, 1);
    }

    public BasketItem add(){
        quantity++;
        return this;
    }

    /**
     * Can't have 0 or else it wouldn't be a basket item
     * @return at least 1
     */
    public BasketItem remove(){
        if(quantity > 1) {
            quantity--;
        }
        return this;
    }
}