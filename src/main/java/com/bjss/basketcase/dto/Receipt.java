package com.bjss.basketcase.dto;

import com.bjss.basketcase.dto.offers.BaseOffer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
public class Receipt extends AbstractReceipt{

    private BigDecimal total;
    private BigDecimal subTotal;
    private List<BaseOffer> offers;

    @Override
    public String toString() {
        return "Subtotal: £" + subTotal + "\n" +
                (offers == null ? "(no offers available)" : offers.stream()
                        .map(BaseOffer::getOffer).map(offer -> offer.getName() + ": -" + offer.getValueWithDiscount()  + "p")
                        .collect(Collectors.joining(" \n"))) + "\nTotal: £" + total;
    }
}