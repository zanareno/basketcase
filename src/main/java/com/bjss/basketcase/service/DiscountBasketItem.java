package com.bjss.basketcase.service;

import com.bjss.basketcase.dto.BasketItem;
import com.bjss.basketcase.dto.common.Price;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.offers.BaseOffer;
import com.bjss.basketcase.dto.offers.OfferDateDiscount;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Getter
public class DiscountBasketItem implements Discount<BasketItem, OfferDateDiscount> {

    private final Map<Product, List<OfferDateDiscount>> offersMap = new HashMap<>();

    @Override
    public void read(final Collection<OfferDateDiscount> discountStrategies) {
        for (final BaseOffer offer : discountStrategies) {
            if (offer instanceof OfferDateDiscount) {
                final Product product = offer.getOffer().getProduct();
                if (!offersMap.containsKey(product)) {
                    offersMap.put(product, new ArrayList<>());
                }
                offersMap.get(product).add((OfferDateDiscount) offer);
            }
        }
    }

    @Override
    public Optional<OfferDateDiscount> selectOffer(final BasketItem basketItem) {
        if (offersMap.isEmpty()) {
            return Optional.empty();
        }
        final List<OfferDateDiscount> offerDateDiscount = offersMap.get(basketItem.getProduct());
        if (offerDateDiscount == null) {
            return Optional.empty();
        }
        return offersMap.get(basketItem.getProduct()).stream().filter(Objects::nonNull)
                .filter(offer -> LocalDateTime.now().isAfter(offer.getTimeRange().getStart()) && LocalDateTime.now().isBefore(offer.getTimeRange().getEnd())).findFirst();
    }

    @Override
    public void apply(final BasketItem basketItem, final Optional<OfferDateDiscount> offer) {
        if (basketItem != null) {
            offer.ifPresent(offerDateDiscount -> {
                final int discount = offerDateDiscount.getOffer().getDiscount();
                basketItem.getProduct().getActive(LocalDate.now()).ifPresent(price -> {
                    final BigDecimal valueToDiscount = getValueToDiscount(discount, price);
                    offerDateDiscount.getOffer().setValueWithDiscount(valueToDiscount);
                    price.setValue(price.getValue().subtract(valueToDiscount));

                });
            });
        }
    }

    private BigDecimal getValueToDiscount(final int discount, final Price price) {
        return price.getValue().multiply(BigDecimal.valueOf(discount)).divide(BigDecimal.valueOf(100));
    }


}