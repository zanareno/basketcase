package com.bjss.basketcase.service;

import com.bjss.basketcase.dto.offers.BaseOffer;

import java.util.Collection;
import java.util.Optional;

public interface Discount<E, S extends BaseOffer> {

    void read(Collection<S> offers);

    Optional<S> selectOffer(final E element);

    void apply(final E element, final Optional<S> offer);
}