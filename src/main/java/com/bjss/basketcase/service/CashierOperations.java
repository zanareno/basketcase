package com.bjss.basketcase.service;

import com.bjss.basketcase.dto.AbstractReceipt;
import com.bjss.basketcase.dto.BasketItem;

import java.util.List;

public interface CashierOperations {

    boolean add(final BasketItem basketItem);
    boolean remove(final BasketItem basketItem);
    AbstractReceipt checkout(final List<String> products);
}