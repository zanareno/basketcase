package com.bjss.basketcase.service;

import com.bjss.basketcase.dto.AbstractReceipt;
import com.bjss.basketcase.dto.Basket;
import com.bjss.basketcase.dto.BasketItem;
import com.bjss.basketcase.dto.NullReceipt;
import com.bjss.basketcase.dto.Receipt;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.offers.BaseOffer;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class Cashier implements CashierOperations {

    private final Map<Product, BaseOffer> offers;
    private final Map<Product, Integer> stock;
    private final Map<String, Product> products;
    private final List<Discount<Basket, BaseOffer>> discountsHoverBasket;
    private final List<Discount<BasketItem, BaseOffer>> discountsHoverBasketItem;

    public Cashier(List<Discount<Basket, BaseOffer>> discountsOverBasket, List<Discount<BasketItem, BaseOffer>> discountsOverBasketItem,
                   final Map<Product, BaseOffer> offers, final Map<Product, Integer> stock, final Map<String, Product> products) {
        this.offers = offers;
        this.stock = stock;
        this.products = products;
        this.discountsHoverBasket = discountsOverBasket;
        this.discountsHoverBasketItem = discountsOverBasketItem;
        loadOffers();
    }

    private void loadOffers() {
        discountsHoverBasket.forEach(d -> d.read(offers.values()));
        discountsHoverBasketItem.forEach(d -> d.read(offers.values()));
    }

    @Override
    public AbstractReceipt checkout(final List<String> request) {
        final Set<BasketItem> basketItems = new HashSet<>();
        final Map<Product, Long> order = request.stream().filter(products::containsKey).map(products::get).collect(Collectors.groupingBy(product -> product, Collectors.counting()));
        if(order.isEmpty())
            return new NullReceipt();

        order.forEach((product, aLong) -> basketItems.add(BasketItem.builder().product(product).quantity(Math.toIntExact(aLong)).build()));

        final BigDecimal subTotal = sum(basketItems);
        final AbstractMap.SimpleEntry<Set<BasketItem>, List<BaseOffer>> firstAppliedDiscount = getNewValuesForBasketItems(basketItems);
        final Basket basket = new Basket();
        firstAppliedDiscount.getKey().forEach(basket::addToBasket);
        final AbstractMap.SimpleEntry<Basket, List<BaseOffer>> discountHoverBasket = getNewValuesForBasketItems(basket);
        final BigDecimal total = sum(new HashSet<>(discountHoverBasket.getKey().getBasketItems().values()));
        final List<BaseOffer> offers = firstAppliedDiscount.getValue();
        offers.addAll(discountHoverBasket.getValue());

        final BigDecimal subTotalL = subTotal.divide(BigDecimal.valueOf(100));
        final BigDecimal totalL = total.divide(BigDecimal.valueOf(100));
        return Receipt.builder().subTotal(subTotalL).total(totalL).offers(offers).build();
    }

    private BigDecimal sum(final Set<BasketItem> basketItems) {
        return basketItems.stream().map(basketItem -> basketItem.getProduct().getActive(LocalDate.now()).get().getValue().multiply(BigDecimal.valueOf(basketItem.getQuantity()))).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private AbstractMap.SimpleEntry<Set<BasketItem>, List<BaseOffer>> getNewValuesForBasketItems(final Set<BasketItem> basketItems) {
        final List<BaseOffer> baseOffers = new ArrayList<>();
        basketItems.stream()
                .forEach(basketItem -> discountsHoverBasketItem.stream()
                        .forEach(basketItemBaseOfferDiscount -> {
                            final Optional<BaseOffer> baseOffer = basketItemBaseOfferDiscount.selectOffer(basketItem);
                            baseOffer.ifPresent(baseOffers::add);
                            basketItemBaseOfferDiscount.apply(basketItem, baseOffer);
                        }));
        return new AbstractMap.SimpleEntry<>(basketItems, baseOffers);
    }

    private AbstractMap.SimpleEntry<Basket, List<BaseOffer>> getNewValuesForBasketItems(final Basket basket) {
        final List<BaseOffer> baseOffers = new ArrayList<>();
        discountsHoverBasket.forEach(basketOfferDiscount -> {
            final Optional<BaseOffer> baseOffer = basketOfferDiscount.selectOffer(basket);
            baseOffer.ifPresent(baseOffers::add);
            basketOfferDiscount.apply(basket, baseOffer);
        });
        return new AbstractMap.SimpleEntry<>(basket, baseOffers);
    }

    @Override
    public boolean remove(final BasketItem basketItem) {
        Integer current = stock.get(basketItem.getProduct());
        current -= basketItem.getQuantity();
        if (current >= 0) {
            stock.put(basketItem.getProduct(), current);
            return true;
        }
        return false;
    }

    /**
     * In case of a refund
     *
     * @param basketItem basketItem
     * @return true if success. False otherwise.
     */
    @Override
    public boolean add(final BasketItem basketItem) {
        return false;
    }
}