package com.bjss.basketcase.service;

import com.bjss.basketcase.dto.Basket;
import com.bjss.basketcase.dto.BasketItem;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.offers.BaseOffer;
import com.bjss.basketcase.dto.offers.OfferDiscountByProductQuantity;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Getter
public class DiscountQuantity implements Discount<Basket, OfferDiscountByProductQuantity> {

    private final Map<Product, List<OfferDiscountByProductQuantity>> offersMap = new HashMap<>();

    @Override
    public void read(final Collection<OfferDiscountByProductQuantity> discountStrategies) {
        for (final BaseOffer offer : discountStrategies) {
            if (offer instanceof OfferDiscountByProductQuantity) {
                OfferDiscountByProductQuantity offerDiscountByProductQuantity = (OfferDiscountByProductQuantity) offer;
                if (!offersMap.containsKey(offerDiscountByProductQuantity.getProduct())) {
                    offersMap.put(offerDiscountByProductQuantity.getProduct(), new ArrayList<>());
                }
                offersMap.get(offerDiscountByProductQuantity.getProduct()).add((OfferDiscountByProductQuantity) offer);
            }
        }
    }

    @Override
    public Optional<OfferDiscountByProductQuantity> selectOffer(final Basket basket) {
        if (offersMap.isEmpty()) {
            return Optional.empty();
        }
        return basket.getBasketItems().entrySet().stream()
                .filter(productBasketItemEntry -> offersMap.containsKey(productBasketItemEntry.getKey()))
                .map(productBasketItemEntry -> offersMap.get(productBasketItemEntry.getKey()).stream()
                        .filter(offerDiscountByProductQuantity -> productBasketItemEntry.getValue().getQuantity() >= offerDiscountByProductQuantity.getQuantity()))
                .flatMap(Stream::distinct)
                .findFirst();
    }

    @Override
    public void apply(final Basket basket, final Optional<OfferDiscountByProductQuantity> offer) {

        offer.ifPresent(offerDiscountByProductQuantity -> {
            final int discount = offerDiscountByProductQuantity.getOffer().getDiscount();
            final BasketItem basketItem = basket.getBasketItems().get(offerDiscountByProductQuantity.getOffer().getProduct());
            if (basketItem != null) {
                basketItem.getProduct().getActive(LocalDate.now()).ifPresent(price -> {
                    final BigDecimal valueToDiscount = getValueToDiscount(discount, price.getValue());
                    offerDiscountByProductQuantity.getOffer().setValueWithDiscount(valueToDiscount);
                    price.setValue(price.getValue().subtract(valueToDiscount));
                });
            }
        });
    }

    private BigDecimal getValueToDiscount(final int discount, final BigDecimal price) {
        return price.multiply(BigDecimal.valueOf(discount)).divide(BigDecimal.valueOf(100));
    }


}