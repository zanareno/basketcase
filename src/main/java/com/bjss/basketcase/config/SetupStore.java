package com.bjss.basketcase.config;

import com.bjss.basketcase.dto.Basket;
import com.bjss.basketcase.dto.BasketItem;
import com.bjss.basketcase.dto.common.Price;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.common.ProductType;
import com.bjss.basketcase.dto.common.Range;
import com.bjss.basketcase.dto.offers.BaseOffer;
import com.bjss.basketcase.dto.offers.Offer;
import com.bjss.basketcase.dto.offers.OfferDateDiscount;
import com.bjss.basketcase.dto.offers.OfferDiscountByProductQuantity;
import com.bjss.basketcase.service.Discount;
import com.bjss.basketcase.service.DiscountBasketItem;
import com.bjss.basketcase.service.DiscountQuantity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum SetupStore {

    INSTANCE;

    public static final List<Price> SOUP_PRICE_LIST = List.of(Price.builder().id(1).dates(new Range<>(LocalDate.MIN, LocalDate.MAX)).value(BigDecimal.valueOf(65)).description("Soup").build());
    public static final List<Price> BREAD_PRICE_LIST = List.of(Price.builder().id(1).dates(new Range<>(LocalDate.MIN, LocalDate.MAX)).value(BigDecimal.valueOf(80)).description("Bread").build());
    public static final List<Price> MILK_PRICE_LIST = List.of(Price.builder().id(1).dates(new Range<>(LocalDate.MIN, LocalDate.MAX)).value(BigDecimal.valueOf(130)).description("Milk").build());
    public static final List<Price> APPLES_PRICE_LIST = List.of(Price.builder().id(1).dates(new Range<>(LocalDate.MIN, LocalDate.MAX)).value(BigDecimal.valueOf(100)).description("Apples").build());

    public static final Map<String, Product> products = new HashMap<>();
    public static final Map<Product, Integer> stock = new HashMap<>();
    public static final Map<Product, BaseOffer> offers = new HashMap<>();
    public static final List<Discount<Basket, BaseOffer>> discountsHoverBasket = new ArrayList(Collections.singletonList(new DiscountQuantity()));
    public static final List<Discount<BasketItem, BaseOffer>> discountsHoverBasketItem = new ArrayList(Collections.singletonList(new DiscountBasketItem()));

    public static Map<String, Product> readProducts() {
        products.put("Soup", Product.builder().sku(1L).name("Soup").prices(SOUP_PRICE_LIST).productType(ProductType.UNIT).build());
        products.put("Bread", Product.builder().sku(2L).name("Bread").prices(BREAD_PRICE_LIST).productType(ProductType.UNIT).build());
        products.put("Milk", Product.builder().sku(3L).name("Milk").prices(MILK_PRICE_LIST).productType(ProductType.UNIT).build());
        products.put("Apples", Product.builder().sku(4L).name("Apples").prices(APPLES_PRICE_LIST).productType(ProductType.UNIT).build());
        products.put("Oranges", Product.builder().sku(5L).name("Oranges").prices(APPLES_PRICE_LIST).productType(ProductType.WEIGHT).build());
        return products;
    }

    public static Map<Product, Integer> readStock() {
        products.values().forEach(product -> stock.put(product, 100));
        return stock;
    }

    public static Map<Product, BaseOffer> readOffers() {
        final OfferDateDiscount applesDiscount = OfferDateDiscount.builder()
                .timeRange(new Range<>(
                        LocalDateTime.now().minusDays(1),
                        LocalDateTime.now().plusWeeks(1)))
                .build();
        final Product apples = products.get("Apples");
        applesDiscount.setOffer(Offer.builder().discount(10).product(apples).name("Apples 10% off").build());
        offers.put(apples, applesDiscount);

        final Product soup = products.get("Soup");
        final Product bread = products.get("Bread");
        final OfferDiscountByProductQuantity productQuantity = OfferDiscountByProductQuantity.builder()
                .product(soup).quantity(2).build();
        productQuantity.setOffer(Offer.builder().discount(50).product(bread).name("2 Soups, 50% off in bread").build());
        offers.put(soup, productQuantity);

        return offers;
    }

}