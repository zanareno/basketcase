package com.bjss.basketcase;

import com.bjss.basketcase.config.SetupStore;
import com.bjss.basketcase.dto.AbstractReceipt;
import com.bjss.basketcase.dto.Receipt;
import com.bjss.basketcase.dto.common.Product;
import com.bjss.basketcase.dto.offers.BaseOffer;
import com.bjss.basketcase.service.Cashier;
import com.bjss.basketcase.service.CashierOperations;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Application {

    final static Map<Product, Integer> stock = SetupStore.readStock();
    final static Map<String, Product> products = SetupStore.readProducts();
    final static Map<Product, BaseOffer> offers = SetupStore.readOffers();

    public static void main(String... args){
        final CashierOperations cashier = new Cashier(SetupStore.discountsHoverBasket, SetupStore.discountsHoverBasketItem, offers, stock, products);

        try(Scanner in = new Scanner(System.in)){
            System.out.print("PriceBasket ");
            String request = in.nextLine();
            final List<String> order = Arrays.stream(request.split(" ")).collect(Collectors.toList());
            final AbstractReceipt receipt = cashier.checkout(order);
            System.out.println(receipt);
        }
    }
}